from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def setup_module(module):
    """ setup any state tied to the execution of the given function.
    Invoked for every test function in the module.
    """
    global driver
    driver = webdriver.Firefox()

def teardown_module(module):
    """ teardown any state that was previously setup with a setup_function
    call.
    """
    global driver
    driver.close()

def test_www():
    global driver
    driver.get("https://fscons.org")
    assert "Free Society Conference and Nordic Summit" in driver.page_source

def test_subscribe_announces():
    global driver
    driver.get("https://fscons.org/")
    driver.find_element_by_name("email").send_keys("foobar@example.net")
    driver.find_element_by_id("subscribe-button").click()
    assert "You've made a subscription request to announces." in driver.page_source
