--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
import Data.Monoid (mappend,(<>))
import Control.Monad (liftM, forM_)
import Hakyll
import System.FilePath ((</>), takeDirectory)
import qualified Text.Regex.TDFA

default (String)

--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do

    -- Load global templates
    match "templates/*" $ compile templateCompiler

    -- 404 page
    copy "404/**"

    -- Robots.txt
    copy "robots.txt"

    -- 2007-2012 sites archived as static html.
    copy "2007/**"
    copy "2008/**"
    copy "2009/**"
    copy "2010/**"
    copy "2011/**"
    copy "2012/**"

    -- The buildSite function is rougthly equivalent to `jekyll build`
    lessc "2013/_less/fscons.less" "2013/css/fscons.css" ["2013/_less/"]
    buildSite "2013" 4

    lessc "2014/_less/fscons.less" "2014/css/fscons.css" ["2014/_less/"]
    buildSite "2014" 2

    lessc "2015/_less/fscons.less" "2015/css/fscons.css" ["2015/bower_components/bootstrap/less/"]
    buildSite "2015" 2

    -- Build feeds from posts from multiple years
    feeds ("2013/_posts/*" .||. "2014/_posts/*" .||. "2015/_posts/*")

    -- Föreningen pages
    foreningen

--------------------------------------------------------------------------------
buildSite :: String -> Int -> Rules ()
buildSite year np = do
    match (fromGlob (year </> "_layouts/*")) $ compile templateCompiler

    -- Static files are just copied
    forM_ ["images", "fonts", "js", "res"] $ \dir -> do
        match (fromGlob (year </> dir </> "**")) $ do
            route   idRoute
            compile copyFileCompiler

    -- Html page are included as-is in the theme template
    match (fromRegex ('^':year </> "[^_].*\\.html$")) $ do
        route   idRoute
        compile $ getResourceBody
            >>= applyAsTemplate yearContext
            >>= loadAndApplyTemplate themeLayout yearContext
            >>= relativizeUrls

    -- Markdown files need to be compiled using pandoc
    match (fromRegex ('^':year </> "[^_].*\\.md$")) $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= applyAsTemplate yearContext
            >>= loadAndApplyTemplate themeLayout yearContext
            >>= relativizeUrls

    -- This creates a page listing all the news
    create [fromFilePath (year </> "news.html")] $ do
        route idRoute
        compile $ do
            let ctx = listField "posts" postCtx allPosts
                    <> constField "title" "News" <> yearContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/news.html" ctx
                >>= loadAndApplyTemplate themeLayout ctx
                >>= relativizeUrls

    create [fromFilePath (year </> "index.html")] $ do
        route   idRoute
        compile $ do
            let ctx = listField "posts" postCtx (liftM (take np) allPosts)
                    <> constField "title" "Home" <> yearContext
            makeItem ""
                >>= loadAndApplyTemplate indexLayout ctx
                >>= loadAndApplyTemplate themeLayout ctx
                >>= relativizeUrls

    match posts $ do
        let context = postCtx <> yearContext
        route (gsubRoute "_posts" (const "news")
            `composeRoutes` setExtension "html")
        compile $ do
            ext <- getUnderlyingExtension
            (if ext == ".html" then getResourceBody else pandocCompiler)
              >>= applyAsTemplate context
              >>= saveSnapshot "pandoc"
              >>= loadAndApplyTemplate newsLayout    context
              >>= saveSnapshot "content"
              >>= loadAndApplyTemplate themeLayout context
              >>= relativizeUrls
  where
    newsLayout  = fromFilePath (year </> "_layouts/news_layout.html")
    themeLayout = fromFilePath (year </> "_layouts/default.html")
    indexLayout = fromFilePath (year </> "_layouts/index.html")
    yearContext = constField "year" year
            <> constField "baseurl"   ('/':year)
            <> constField "schedule" (case year of
                "2014" -> "https://frab.fscons.org/en/fscons14/public/schedule"
                _ -> "https://frab.fscons.org/en/fscons" <> year <> "/public/schedule/0")
            <> constField "blog"      "http://blog.fscons.org/"
            <> constField "wiki"      "http://wiki.fscons.org/"
            <> yearListField [2007..2015]
            <> defaultContext
    posts       = fromGlob (year </> "_posts/*")
    allPosts    = recentFirst =<< loadAllSnapshots posts "pandoc"

copy :: Pattern -> Rules ()
copy glob = match glob (route idRoute >> compile copyFileCompiler)


lessc :: FilePath -> Identifier -> [FilePath] -> Rules ()
lessc src dst includes = do
    -- Tell hakyll to watch the less files in the directory
    match lessFiles $ compile getResourceBody
    -- Compile the main less file
    -- We tell hakyll it depends on all the less files,
    -- so it will recompile it when needed
    d <- makePatternDependency lessFiles
    rulesExtraDependencies [d] $ create [dst] $ do
        route idRoute
        compile $ loadBody (fromFilePath src)
            >>= makeItem
            >>= withItemBody
              (unixFilter "./node_modules/less/bin/lessc" ("-":lesscArgs))
  where
    dir = takeDirectory src
    lessFiles = fromGlob (dir </> "*.less")
    lesscArgs = [ "--include-path=" ++ d | d <- includes ]


postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    snipetField "snipet" <>
    metadataField <>
    defaultContext

yearListField :: [Int] -> Context b
yearListField years = listField "years" yearContext (mapM makeItem years)
  where
    yearContext = field "url" (\i -> return ("/" ++ show (itemBody i) ++ "/"))
                <> field "title" (return . ("FSCONS'"++) . drop 2 . show . itemBody)

feeds :: Pattern -> Rules ()
feeds pattern = do
    create ["atom.xml"] $ do
        route idRoute
        compile $ getPosts >>= renderAtom config ctx
    create ["rss.xml"] $ do
        route idRoute
        compile $ getPosts >>= renderRss config ctx
  where
    getPosts =
        liftM (take 10) (loadAllSnapshots pattern "content" >>= recentFirst)
    ctx = postCtx <> bodyField "description"
    config = FeedConfiguration
        { feedTitle       = "FSCONS"
        , feedDescription = "All news posts from fscons.org"
        , feedAuthorName  = "The FSCONS team"
        , feedAuthorEmail = "info@fscons.org"
        , feedRoot        = "http://fscons.org"
        }


------------------------------------------------------------------------------
-- | Föreningen pages.
foreningen :: Rules ()
foreningen = do
    match "foreningen/*/*" $ do
        route idRoute
        compile copyFileCompiler

    match "foreningen/index.html" $ do
        route idRoute
        let minuteCtx = dateField "date" "%B %e, %Y" -- <> defaultContext
                      <> urlField "url" -- <> titleField "title"
                      <> minuteTypeField "title"
            minutes = recentFirst =<< loadAll "foreningen/20*/*" :: Compiler [Item CopyFile]
            ctx = listField "minutes" minuteCtx minutes
        compile $ getResourceBody >>= applyAsTemplate ctx >>= relativizeUrls

    create ["foreningen/rss.xml"] $ do
        route idRoute
        compile $ getMinutes >>= renderRss config ctx >>= relativizeUrls
  where
    getMinutes :: Compiler [Item String]
    getMinutes = do
        items <- recentFirst =<< loadAll "foreningen/20*/*" ::  Compiler [Item CopyFile]
        return (fmap (itemSetBody "") items)
    ctx = dateField "date" "%B %e, %Y"
        <> urlField "url"
        <> minuteTypeField "title"
        <> constField "description" ""
    config = FeedConfiguration
        { feedTitle       = "Föreningen FSCONS"
        , feedDescription = "Föreningen FSCONS meeting minutes"
        , feedAuthorName  = "The FSCONS team"
        , feedAuthorEmail = "info@fscons.org"
        , feedRoot        = "http://fscons.org"
        }

--------------------------------------------------------------------------------
---- | Constructs a 'field' that contains the body of the item.
snipetField :: String -> Context String
snipetField key = field key $ return . unwords . take 20 . words . stripTags . itemBody

-- | Pretty version of the minute type base on the file name
minuteTypeField :: String -> Context a
minuteTypeField key = field key $ \item -> return $
  case toFilePath (itemIdentifier item) of
    s | s =~ "board"            -> "Board meeting"
    s | s =~ "(yearly|annual)"  -> "Annual meeting"
    s | s =~ "eval"             -> "Evaluation meeting"
    s | s =~ "constitutional"   -> "Constitutional meeting"
    s                           -> s
  where (=~) = (Text.Regex.TDFA.=~) :: String -> String -> Bool

