---
layout: news_layout
title: CFP deadline extended
tags: 2014, venue
author: The FSCONS team
---

Due to some technical issues which prevented some speakers from
submitting their talks, we have decided to extend the CfP deadline for
FSCONS 2014 by two weeks, to 4 July.

[Read the CfP]($baseurl$/cfp.html)
