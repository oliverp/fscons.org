FSCONS Manifesto
================

*This is the official Manifesto for FSCONS. The association appreciates
your feedback: Please discuss and give us your input, reflections and
suggestions in the [wiki](https://wiki.fscons.org/FSCONS_Manifesto).*

Purpose
-------

Föreningen FSCONS exists to provide a meeting place where subjects
relating to freedom within society, culture and technology can be
brought to life in peer discussions. Föreningen FSCONS should provide a
forum where people, organisations and public sector, with interest in
the three subject areas can meet in a participatory and constructive
dialogue. The combination of topics creates a platform where new
co-operations and thoughts can emerge which allows the participants to
find new sources of inspiration.

Background
----------

FSCONS stands for Free Society Conference and Nordic Summit. While
initially founded as Free Software conference *Scandinavia*, the
conference has evolved into a yearly conference with a much broader
scope - focusing on the interaction between culture, society and
technology. The conference was first held in 2007 as a small but focused
free software conference in Gothenburg. In 2012 the non-profit
association *Föreningen FSCONS* was founded. Since then, the members of
the association organise the conference FSCONS, in collaboration with
other organisations.

Foundations
-----------

1.  The participants make the conference:\
     FSCONS should generate new ideas and the interaction between peers
    is important. Everyone contributes according to their ability.
    -   Volunteers are participants:\
         Volunteers are participants who work during scheduled shifts as
        part of their contribution to the conference.
    -   Speakers are participants:\
         Speakers are participants who give scheduled presentations as
        part of their contribution to the conference.

2.  Participants respect each other:\
     We are non-discriminatory and accessible. We take an active stand
    for everyone's equal value, fundamental rights and freedoms. All
    participants are equally welcome. We do not tolerate harassment in
    any form.
3.  Föreningen FSCONS wants low barriers for participation:\
     We actively work to make sure that our activities are accessible to
    everyone, regardless of experience, physiology, social class etc. We
    are open, transparent and inclusive in how we work, and what we do.
4.  FSCONS lets different opinions interact:\
     We encourage a constructive dialogue between different
    stakeholders. The content should challenge the participants' views,
    and encourage new thinking. We must dare to approach and talk about
    new subjects.
5.  Föreningen FSCONS works for sustainability:\
     All work carried out by Föreningen FSCONS should contribute towards
    a sustainable society and ecology.
6.  Föreningen FSCONS is more than a conference:\
     Föreningen FSCONS wants to promote discussions of freedom, equality
    and change within the areas of society, culture and technology
    throughout the year, not just at the annual conference.

Goals
-----

Below are the goals that Föreningen FSCONS have set for the annual
conference. The goals should ideally be measurable, identifiable and
attainable.

### List of goals

FSCONS should:

1.  equally address the following subjects:
    -   *Society*, including integrity, privacy, freedom of speech,
        human rights and access to knowledge,
    -   *Culture*, including visual arts and crafts, literature,
        performing arts and the broad spectrum of newer cultural
        expressions,
    -   *Technology*, including software and hardware in a broad
        meaning, as well as questions of infrastructure and networks

2.  spark interesting discussions and co-operations
3.  have an equal gender distribution among participants, within a 60/40
    interval
4.  be accessible in accordance with the guidelines proposed by
    *Myndigheten för Delaktighet*
5.  have an awareness of HBTQ issues among participants, based on the
    guidelines proposed by *RFSL*
6.  counteract discrimination of participants due to region of residence
    or origin
7.  have low barriers for participation, encourage first-time
    participants and incorporate their experiences
8.  work for a sustainable society and ecology, in accordance with the
    guidelines proposed by *Naturskyddsföreningen*
9.  be financially self-supporting, while setting aside funds for
    side-events and future activities of the association

### Approaches to accomplishing the goals

1.  Organise social events, with discussions and informal talks
2.  Invite the public sector, non-governmental organisations,
    corporations and other organisations to contribute to FSCONS, to
    incorporate FSCONS in their internal training programs, and to take
    part in the ongoing discussion on culture, society and technology
3.  Collaborate with organizations working for equality, such as and
    *Kvinnofolkhögskolan*, *Geek Girl Meetup* and *Duchess Sweden*
4.  Collaborate with organizations working with disability issues, such
    as *DHR*; continue collaboration with *Nordiska Folkhögskolan*
5.  Collaborate with organizations working with HBTQ issues, such as
    *RFSL*
6.  Collaborate with organizations working against discrimination based
    on region of residence or origin, such as *Stiftelsen Expo*
7.  Invite people to participate in the organization and implementation
    of FSCONS
8.  Establish a system of mentors who guide and involve new participants
    in FSCONS
9.  In the interest of long-term sustainability, adopt policies and
    approaches of established organisations when available; prioritise
    member run organisations over larger organisations
10. When interacting with other organisations, reach out to members
    deeper in their hierarchy to give them the opportunity to gain
    experience, and to alleviate the pressure on high-profile
    representatives
11. Recycle and reuse materials, and choose local and sustainable
    alternatives whenever possible in order to minimise our ecological
    footprint
12. Collaborate with organisations working for sustainability, such as
    *Naturskyddsföreningen*; continue collaboration with
    *Omställningsrörelsen*
13. Encourage sharing of ideas to promote a more equal and sustainable
    society
14. Establishing a dialogue with the sponsors of FSCONS
15. Measure and evaluate our approaches in terms efficiency and impact
    in meeting the goals

