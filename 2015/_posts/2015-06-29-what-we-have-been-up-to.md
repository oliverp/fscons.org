---
title: What we have been up to
author: Oliver Propst
---

Time flies, it's already June. Here is an update on some of the things we have
been up to since FSCONS 2014.

* Based on the results of the evaluation meetings in 2014 we are this year
  having working meeting on [IRC][1]  (#fscons at freenode.org) the third
  Monday of every month. Keep an eye on the [mailing list][2] for
  reminders/announcements. We have so far had 4 IRC meetings.
* On the yearly meeting in March a new association board was elected and a new
  version of our [manifesto][3] was accepted.
* 7-8 November have been decided as dates for FSCONS 2015. Mark your calendar!

In addition to this the planning aspects have been busy starting preparing the
conference. You might have noticed that we just recently announced the
[2015 CFP] [4] and we are about to announce more updates. Thanks to Grégoire
Détrez [fscons.org] [5] now reflects the current state of the conference.

We are as always looking for people who are interested in
[getting involved][6], either in the planning or as on-site volunteers during
the conference. Let us know if you are interested. Much of the discussion and
coordination regarding the planning of FSCONS still takes place on our
[mailing list][2].

[1]: https://en.wikipedia.org/wiki/Internet_Relay_Chat "IRC"
[2]: https://lists.fripost.org/lists.fscons.org/sympa/info/org "mailing list"
[3]: https://fscons.org/2015/manifesto.html "manifesto"
[4]: https://fscons.org/2015/cfp.html "CFP"
[5]: https://fscons.org/2015/ "FSCONS.org"
[6]: https://fscons.org/2015/get-involved.html
[7]: mailto:volunteer@fscons
