---
title: Registration is now open
author: Oliver Propst
---

Registration is now open
We have now opened the [registration][1] for FSCONS 2015.

The rates this year are as follow
Early bird: 450 SEK (Available until September 30th )
Regular: 700 SEK
Professional: 1,900 SEK
 
Lunch is this year included in the registration fee and will be served at the venue. In other words, no need for rushing outside to grab something to eat.

The dates for FSCONS 2015 are as previously announced 7-8 November. Like the last two years FSCONS 2015 will take place at the Gothenburg University Faculty of Arts venue [Humanisten][2]. Stay tuned for further updates.

[1]: https://fscons.org/2015/register.html
[2]: http://hum.gu.se/english



