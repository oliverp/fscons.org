---
title: Early-bird registrations extended
author: Grégoire Détrez
---

Due to popular demand, we have extended the deadline for the early-bird tickets
to the 25th of October. Grab yours on the [registration](/2015/register.html)
page.
