---
title: FSFE, Mozilla and Debian to participate at FSCONS 2015
author: Oliver Propst
---

One of the great things with attending FSCONS is the possibility to meet
organizations and interact with people active in your favorite Free Software
and Open Source project/organization.

This year (2015) we're to glad to have representatives from [Mozilla][1],
[Debian][2], and [FSFE][3],  present.

![FSFE logo]($baseurl$/images/fsfe_logo.png)
![Mozilla logo]($baseurl$/images/mozilla_logo.png)
![Debian logo]($baseurl$/images/debian_logo.png)

To register for FSCONS 2015 visit https://fscons.org/2015/register.html

[1]: https://www.mozilla.org/ "Mozilla"
[2]: https://www.debian.org/   "Debian"
[3]: http://fsfe.org/ "FSFE"
