$().ready(function () {
    $("[data-icon]").each(function(index, value) {
        var $value = $(value),
            icon = $value.attr("data-icon");
        $value.addClass("add-icon");
        $value.css("background-image", "url(" + icon + ")");
    });

    // Dynamically hide/expand paragraphs with "more" link
    $('p.more').each(function() {
        var numchar = 200; // Number of characters to show
        var content = $(this).html();

        if(content.length > numchar) {
            var c = content.substr(0, numchar);
            var h = content.substr(numchar-1, content.length - numchar);
            $(this).html(c);
            $(this).append('<span>&#8230;&nbsp;</span>');
            $(this).append('<span style="display:none;">' + h + '&nbsp;&nbsp;</span>');
            $(this).append('<a href="" class="morelink">more</a>');

        }
    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).text(">> more");
        } else {
            $(this).addClass("less");
            $(this).text("<< less");
        }
        $(this).prev().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
