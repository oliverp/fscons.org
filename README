==============================================================================
fscons.org
==============================================================================

This is the static website for fscons. Other, more interactive part (schedule,
tickets, wiki...), are handled elsewhere.

This project uses hakyll but the only thing you need to build it is a
(relatively recent) haskell platform.

    cabal sandbox init
    cabal install
    cabal run -- build

The generated pages end up in _site/

There is also the option of using hakyll build-in web server which will rebuild
pages when a source file changes (http://localhost:8000):

    cabal run -- watch

Features
========

- Every year can have its own set of templates
- Global rss feed for news posts. (all years mixed)

Contribute
==========

- Issue Tracker: https://gitlab.com/fscons/fscons.org/issues
- Source Code: https://gitlab.com/fscons/fscons.org/

Icons
-----

Most of the icons come from http://thenounproject.com

They should be converted to png and resized to 50x32
To create an icon of the expected size with imagemagick (from a svg, probably
other format as well)

    convert -resize x32 -extent 50x -gravity center -background none noun_project_14383.svg ic_schedule.png

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: org@lists.fscons.org
