---
layout: news_layout
title: Most videos uploaded
author: The FSCONS team
status: publish
type: post
published: true
---

Almost all the recordings of the talks at FSCONS 2013 have been encoded and
uploaded. They are available both in a free video format at
[videos.fscons.org](http://videos.fscons.org/fscons/2013/)
and on [Youtube](https://www.youtube.com/user/FSCONSTalks).

Thanks to everyone who recorded, sorted, encoded, uploaded and edited the videos
and their metadata!
